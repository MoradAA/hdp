#!/bin/bash
echo "Running Ansible install"

sudo apt-get update -y

sudo apt-get upgrade -y

sudo apt-get install software-properties-common -y

sudo apt-add-repository --yes --update ppa:ansible/ansible

sudo apt-get install -y ansible

sudo mkdir /etc/ansible/playbooks