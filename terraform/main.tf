//Create folder in S3-bucket to save terraform state
terraform {
  backend "s3" {
    bucket = "morad-hdp"
    key    = "terraform/state.tfstate"
    region = "eu-west-1"
  }
}

//Select AWS region
provider "aws" {
  region = "eu-west-1"
}

resource "aws_instance" "ansible" {
  # Ubuntu 18.04
  ami = "ami-095b735dce49535b5"
  instance_type = "t2.micro"
  key_name = "sshkeypair"
  vpc_security_group_ids = ["sg-0be7f5a1226efcbf9"]
  subnet_id = "subnet-020e56aae0b0ee0b2"

  tags = {
    Name = "Ansible"
  }

  user_data = "${file("ansible-installation.sh")}"
}

resource "aws_instance" "masternodes" {
  # Ubuntu 18.04
  ami = "ami-095b735dce49535b5"
  instance_type = "t2.micro"
  # count = 1
  key_name = "sshkeypair"
  #vpc_security_group_ids = ["sg-0be7f5a1226efcbf9"]
  vpc_security_group_ids = ["sg-037a086d93ab574d8"]
  subnet_id = "subnet-020e56aae0b0ee0b2"

  root_block_device {
    volume_size = 30
  }

  tags = {
    Name = "Ambari server"
  }
}

resource "aws_instance" "datanodes" {
  # Ubuntu 18.04
  ami = "ami-095b735dce49535b5"
  instance_type = "t2.micro"
  # count = 2
  key_name = "sshkeypair"
  vpc_security_group_ids = ["sg-0be7f5a1226efcbf9"]
  subnet_id = "subnet-020e56aae0b0ee0b2"

  root_block_device {
    volume_size = 30
  }

  tags = {
    Name = "Ambari agent"
  }
}

resource "time_sleep" "wait_120_seconds" {
  depends_on = [aws_instance.ansible]
  create_duration = "120s"
}

resource "null_resource" "remoteAnsible" {
  depends_on = [time_sleep.wait_120_seconds]

  provisioner "remote-exec" {
    inline = [
      "echo '[masternodes]\n${aws_instance.masternodes.private_ip}\n\n[datanodes]\n${aws_instance.datanodes.private_ip}' | sudo tee -a /etc/ansible/hosts"
    ]
  }

  provisioner "file" {
    source      = "C:\\Users\\morad\\Desktop\\HDP\\ansible\\playbooks\\master.yml"
    destination = "/home/ubuntu/master.yml"
  }

  provisioner "file" {
    source      = "C:\\Users\\morad\\Desktop\\HDP\\ansible\\playbooks\\all.yml"
    destination = "/home/ubuntu/all.yml"
  }

  provisioner "file" {
    source      = "C:\\Users\\morad\\Desktop\\HDP\\ansible\\playbooks\\server.yml"
    destination = "/home/ubuntu/server.yml"
  }

  provisioner "file" {
    source      = "C:\\Users\\morad\\Desktop\\HDP\\ansible\\playbooks\\agent.yml"
    destination = "/home/ubuntu/agent.yml"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /home/ubuntu/*.yml /etc/ansible/playbooks/"
    ]
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = "${file("C:\\Users\\morad\\Desktop\\HDP\\sshkeypair.pem")}"
    host        = "${aws_instance.ansible.public_ip}"
  }
}

resource "null_resource" "remoteMaster" {
  provisioner "remote-exec" {
    inline = [
      "sudo sed -i '/^# The.*/i ${aws_instance.masternodes.private_ip} ${aws_instance.masternodes.private_dns}' /etc/hosts",
      "sudo sed -i '/^# The.*/i ${aws_instance.datanodes.private_ip} ${aws_instance.datanodes.private_dns}' /etc/hosts"
    ]
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = "${file("C:\\Users\\morad\\Desktop\\HDP\\sshkeypair.pem")}"
    host        = "${aws_instance.masternodes.public_ip}"
  }
}

resource "null_resource" "remoteData" {
  provisioner "remote-exec" {
    inline = [
      "sudo sed -i '/^# The.*/i ${aws_instance.masternodes.private_ip} ${aws_instance.masternodes.private_dns}' /etc/hosts",
      "sudo sed -i '/^# The.*/i ${aws_instance.datanodes.private_ip} ${aws_instance.datanodes.private_dns}' /etc/hosts"
    ]
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = "${file("C:\\Users\\morad\\Desktop\\HDP\\sshkeypair.pem")}"
    host        = "${aws_instance.datanodes.public_ip}"
  }
}