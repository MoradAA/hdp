# HDP

Automation of HDP deployment

## Steps

- "terraform apply"
- Provide SSH access from Ansible control instance to cluster instances
- "ansible-playbook master.yml"

## Next
Provide Ambari blueprint that will automate the setup of an actual HDP cluster.
