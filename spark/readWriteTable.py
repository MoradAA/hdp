from pyspark.context import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql import HiveContext

sc = SparkContext("local", "Copy table to another table")
spark = SparkSession.builder.getOrCreate()
hc = HiveContext(sc)

flights = hc.table("default.flights")
flights.write.format("orc").saveAsTable("flights2")