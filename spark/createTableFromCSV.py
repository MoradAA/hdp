from pyspark.sql import SparkSession
from pyspark.context import SparkContext
from pyspark.sql import HiveContext
from pyspark.sql.types import *
from pyspark.sql import Row

sc = SparkContext("local", "Create table from CSV")
spark = SparkSession.builder.getOrCreate()

csv_data = spark.sparkContext.textFile("file:///home/hdfs/datasets/flights/flights.csv")
csv_data  = csv_data.map(lambda p: p.split(","))

header = csv_data.first()
csv_data = csv_data.filter(lambda p:p != header)
df_csv = csv_data.toDF().withColumnRenamed("_1","callsign").withColumnRenamed("_2","number").withColumnRenamed("_3","icao24").withColumnRenamed("_4","registration") \
        .withColumnRenamed("_5","typecode").withColumnRenamed("_6","origin").withColumnRenamed("_7","destination").withColumnRenamed("_8","firstseen").withColumnRenamed("_9","lastseen").withColumnRenamed("_10","day") \
        .withColumnRenamed("_11","latitude_1").withColumnRenamed("_12","longitude_1").withColumnRenamed("_13","altitude_1").withColumnRenamed("_14","latitude_2").withColumnRenamed("_15","longitude_2").withColumnRenamed("_16","altitude_2")

hc = HiveContext(sc)
df_csv.write.format("orc").saveAsTable("flights")